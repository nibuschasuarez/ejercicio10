import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-cuestionario',
  templateUrl: './cuestionario.component.html',
  styleUrls: ['./cuestionario.component.css']
})
export class CuestionarioComponent implements OnInit {
   
  cuestionario!: FormGroup;
  Pregunta1: string ='1. A usted le gusta las peliculas de terror?';
  Pregunta2:string ='2.Que color de boligrafo usa?';
  Pregunta3:string='3.De que color es el caballo blanco de Napoleon?';
  Pregunta4: string='4.Que es mas pesado, 1 kilo de plumas o  1 kilo de oro';
  Pregunta5:string='5. Cuantos dedos tiene una gallina un su pata?';

  

  constructor(private fb: FormBuilder) {
    this.CrearCuestionario();
   }

  ngOnInit(): void {
  }
  get aceptarNoValido(){
    return this.cuestionario.get('aceptar')?.invalid && this.cuestionario.get('aceptar')?.touched
  }

  CrearCuestionario(): void{
    console.log('cues');
    
    this.cuestionario = this.fb.group({
      res1:[''],
      res2:[''],
      res3:[''],
      res4:[''],
      res5:[''],
      aceptar:['']
         
    });
  }

  guardar():void{
    console.log(this.cuestionario.value);
    console.log(`${this.Pregunta1}\nR.-${this.cuestionario.value.res1}`); 
    console.log(`${this.Pregunta2}\nR.-${this.cuestionario.value.res2}`);
    console.log(`${this.Pregunta3}\nR.-${this.cuestionario.value.res3}`);
    console.log(`${this.Pregunta4}\nR.-${this.cuestionario.value.res4}`); 
    console.log(`${this.Pregunta5}\nR.-${this.cuestionario.value.res5}`);
    
    }

}
