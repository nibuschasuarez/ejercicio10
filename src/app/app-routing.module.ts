import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CuestionarioComponent } from './components/cuestionario/cuestionario.component';

const routes: Routes = [
  { path:'cuestionario', component: CuestionarioComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'cuestionario' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
